## About importexport

Import from XML file and JSON URL, save them in database. And then, export it to Excel.

## Requirements

- PHP: ^7.0
- PhpSpreadsheet: ^1.2
- PHP extension php_zip enabled
- PHP extension php_xml enabled
- PHP extension php_gd2 enabled

## Install

```
cd /path/to/your/repo
git clone https://bondaruk@bitbucket.org/bondaruk/importexport.git
```

### .emv

- REMOTE_USER_JSON=https://url.com/users
- REMOTE_USER_XML_DIR=file
- REMOTE_USER_XML_FILE=data.xml

### Console

```bash
composer install
composer dump-autoload -o
php artisan optimize:clear
php artisan migrate:refresh --seed
```

## Use by console

```bash
php artisan user-export:excel
```

## Error

Error JSON = URL not found
Error XML = File not found

## Test

```bash
vendor/bin/phpunit
```

## Packages

- [Guzzle](http://docs.guzzlephp.org/en/stable/)
- [Laravel Excel](https://laravel-excel.com/)


## JSON File

The name is set to .env

```https://url.com/users```

```json
[
  {
    "id": 1,
    "name": "Leanne Grf",
    "username": "Bret",
    "email": "Sincere@sdfs.biz",
    "address": {
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "1-770-736-8031 24242",
    "website": "dfrger.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
  {
    "id": 2,
    "name": "Ervin GKJG",
    "username": "Antonette",
    "email": "Shanna@rf.tv",
    "address": {
      "street": "Victor Erg",
      "suite": "Suite 879",
      "city": "Wisokyburgh",
      "zipcode": "90533",
      "geo": {
        "lat": "-43.9509",
        "lng": "-34.4618"
      }
    },
    "phone": "010-692-6593 x09125",
    "website": "dfd.net",
    "company": {
      "name": "Deckow-Crist",
      "catchPhrase": "Proactive didactic contingency",
      "bs": "synergize scalable supply-chains"
    }
  }
]
```

## XML File

The name is set to .env

```xml
<?xml version="1.0"?>
<readings>
	<reading clientID="1" name="Taylor dfgd" phone="463-170-9623 x156" company="drge">Taylor.dfgd@gmail.com</reading>
	<reading clientID="2" name="Billy fdr" phone="493-170-9623 x156" company="Kock rfer">Billy.fdr@gmail.com</reading>
</readings>
```

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
