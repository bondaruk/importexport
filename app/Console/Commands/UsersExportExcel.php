<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RemoteUserJson;
use App\Models\RemoteUserXml;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\User;

class UsersExportExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user-export:excel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export user list to Excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $status['json'] = [];
        $status['xml'] = [];

        $json = RemoteUserJson::read();
        if (!$json) {
            $this->error('Error JSON');
        } else {
            if (!is_null($json)) {
                $status['json'] = $this->__loopArraySave($json);
                unset($json);
            }
        }

        $xml = RemoteUserXml::read();
        if (!$xml) {
            $this->error('Error XML');
        } else {
            if (!is_null($xml)) {
                $status['xml'] = $this->__loopArrayXml($xml);
                unset($xml);
            }
        }

        Excel::store(new UsersExport, env('REMOTE_USER_XML_DIR') . DIRECTORY_SEPARATOR . 'users.xlsx');

        $result = 'Ok';

        $this->info($result);
    }

    private function __existsOrCreate($array)
    {
        if( isset($array['email']) ){
            $users = User::where('email', $array['email'])->first();

            if( isset($users) ){
                $array['id'] = $users->id;
                if(User::edit($array)) {
                    return 'update';
                } else {
                    return 'error_update';
                }
            } else {
                if(User::create($array)) {
                    return 'create';
                } else {
                    return 'error_create';
                }
            }

        }

        return false;
    }

    private function __loopArraySave(array $array)
    {
        $status['error'] = 0;
        $status['create'] = 0;
        $status['update'] = 0;
        $status['error_create'] = 0;
        $status['error_update'] = 0;

        if(count($array)>0){
            foreach ($array as $k => $value) {
                $out = $this->__existsOrCreate($value);
                if (!$out) {
                    $out = 'error';
                }
                $status[$out] = $status[$out] + 1;
            }
        } else {
            $status['error'] = 1;
        }

        return $status;
    }

    private function __parserXml(array $array)
    {
        if (!isset($array['address'])) {
            $out['name'] = $array['name'];
            $out['username'] = null;
            $out['email'] = $array['email'];
            $out['phone'] = $array['phone'];
            $out['website'] = null;
            $out['password'] = substr(md5(mt_rand()), 0, 59);
            $out['address']['street'] = null;
            $out['address']['suite'] = null;
            $out['address']['city'] = null;
            $out['address']['zipcode'] = null;
            $out['address']['geo']['lat'] = null;
            $out['address']['geo']['lng'] = null;
            $out['company']['name'] = $array['company'];
            $out['company']['catchPhrase'] = null;
            $out['company']['bs'] = null;

            return $out;
        }
        return $array;
    }

    private function __loopArrayXml(array $array)
    {
        $out = [];
        $out_save = [];
        if(count($array)>0){
            foreach ($array as $k => $v) {
                $out[] = $this->__parserXml($v);

            }
            $out_save[] = $this->__loopArraySave($out);
            return $out_save;
        }

        return $array;
    }
}
