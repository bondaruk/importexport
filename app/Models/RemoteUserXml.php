<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RemoteUserXml extends Model
{
    public static function checkXml($xml)
    {
        if (!isset($xml->reading)) {
            return false;
        }
        foreach($xml->reading as $a => $b) {
            if (!isset($b->attributes()->clientID)) {
                return false;
            }
            if (!isset($b->attributes()->name)) {
                return false;
            }
            if (!isset($b->attributes()->phone)) {
                return false;
            }
            if (!isset($b->attributes()->company)) {
                return false;
            }
        }

        return true;
    }

    public static function attributes($data)
    {
        $c['email']   = (string) $data;

        $c['clientID']= (string) $data->attributes()->clientID;
        $c['name']    = (string) $data->attributes()->name;
        $c['phone']   = (string) $data->attributes()->phone;
        $c['company'] = (string) $data->attributes()->company;

        return $c;
    }

    /****************
     * Remote data
     */
    public static function read()
    {
        $directory_file = env('REMOTE_USER_XML_DIR') . DIRECTORY_SEPARATOR . env('REMOTE_USER_XML_FILE');
        $data = [];

        if( Storage::disk('local')->exists($directory_file) ) {
            $contents = Storage::disk('local')->get($directory_file);

            $xml = simplexml_load_string($contents);

            if( RemoteUserXml::checkXml($xml) ){
                foreach($xml as $a => $b) {
                    $data[] = RemoteUserXml::attributes($b);
                }

                return $data;
            }
        }
        return false;
    }
}
