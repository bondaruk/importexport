<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Geo extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'geos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lat', 'lng'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'lat', 'lng', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /****************
     * Attributes
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];


    /****************
     * Relations
     */

    // public function address()
    // {
    //     return $this->belongsTo(Address::class, 'address_id', 'id')->withTimestamps();
    // }

    /****************
     * Scopes
     */

    /****************
     * Queries
     */

    public static function create($data)
    {
        $geo = new Geo();

        $geo->lat = $data['address']['geo']['lat'];
        $geo->lng = $data['address']['geo']['lng'];

        if( ! $geo->save() ) {
            return false;
        }

        return $geo;
    }

    public static function edit($data, $id)
    {
        $geo = Geo::where('id', $id)->first();

        $geo->lat = $data['address']['geo']['lat'];
        $geo->lng = $data['address']['geo']['lng'];

        if( ! $geo->save() ) {
            return false;
        }

        return $geo;
    }

    public static function parser(array $data)
    {
        $geo['address']['geo']['lat'] = null;
        $geo['address']['geo']['lng'] = null;
        return $geo;
    }

}
