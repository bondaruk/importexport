<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Models\Company;
use App\Models\Address;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'phone', 'website', 'password', 'address_id', 'company_id'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'name', 'username', 'email', 'phone', 'website', 'address_id', 'company_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /****************
     * Attributes
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'email_verified_at'
    ];

    /****************
     * Relations
     */
    public function address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id'); //->withTimestamps();
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id'); //->withTimestamps();
    }

    /****************
     * Scopes
     */

    /****************
     * Queries
     */

    public static function read($id = null)
    {
        if (isset($id)) {
            // return User::with(['company', 'address'])
            //                 ->where('id',$id)->first();
        }

        return User::where('user_id',$request->User()->id)->get();


        return User::with(['company', 'address'])->get();

        return User::select('name', 'email', 'phone', 'company_id')->orderBy('created_at', 'DESC')->get();



    }

    public static function create($data)
    {
        $user = new User();

        $geo = Geo::create($data);
        if( ! $geo ) {
            return false;
        }

        $address = Address::create($data, $geo->id);
        if( ! $address ) {
            return false;
        }

        $company = Company::create($data);
        if( ! $company ) {
            return false;
        }

        $user->password = substr(md5(mt_rand()), 0, 59);
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->website = $data['website'];
        $user->address_id = $address->id;
        $user->company_id = $company->id;

        if( ! $user->save() ) {
            return false;
        }
        return $user;
    }

    public static function edit($data)
    {
        $user = User::where('email', $data['email'])->first();

        $address = Address::edit($data, $user->address_id);
        if( ! $address ) {
            return false;
        }

        $company = Company::edit($data, $user->company_id);
        if( ! $company ) {
            return false;
        }

        $user->password = substr(md5(mt_rand()), 0, 59);
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->website = $data['website'];
        $user->address_id = $address->id;
        $user->company_id = $company->id;

        if( ! $user->save() ) {
            return false;
        }
        return $user;
    }

    public static function parser(array $data)
    {
        $user['name'] = null;
        $user['username'] = null;
        $user['email'] = null;
        $user['phone'] = null;
        $user['website'] = null;
        $user['password'] = substr(md5(mt_rand()), 0, 59);

        if (isset($data['name'])) {
            $user['name'] = $data['name'];
        }
        if (isset($data['username'])) {
            $user['username'] = $data['username'];
        }
        if (isset($data['email'])) {
            $user['email'] = $data['email'];
        }
        if (isset($data['phone'])) {
            $user['phone'] = $data['phone'];
        }
        if (isset($data['website'])) {
            $user['website'] = $data['website'];
        }
        if (isset($data['password'])) {
            $user['password'] = $data['password'];
        }

        return $user;
    }

}
