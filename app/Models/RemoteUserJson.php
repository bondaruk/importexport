<?php

namespace App\Models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class RemoteUserJson extends Model
{
    /****************
     * Remote data
     */
    public static function read()
    {
        $client = new Client();

        $response = $client->request('GET', env('REMOTE_USER_JSON'));
        // return $contents = (string) $response->getBody();

        return json_decode($response->getBody(), true);
    }
}
