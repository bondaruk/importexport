<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class Company extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'catchPhrase', 'bs'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'name', 'catchPhrase', 'bs'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /****************
     * Attributes
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /****************
     * Relations
     */

    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }

    /****************
     * Scopes
     */

    /****************
     * Queries
     */

    public static function create($data)
    {
        $company = new Company();

        $company->name = $data['company']['name'];
        $company->catchPhrase = $data['company']['catchPhrase'];
        $company->bs = $data['company']['bs'];
        if( ! $company->save() ) {
            return false;
        }

        return $company;
    }

    public static function edit($data, $id)
    {
        $company = Company::where('id', $id)->first();

        $company->name = $data['company']['name'];
        $company->catchPhrase = $data['company']['catchPhrase'];
        $company->bs = $data['company']['bs'];
        if( ! $company->save() ) {
            return false;
        }

        return $company;
    }

    public static function parser(array $data)
    {
        $company['company']['name'] = null;
        $company['company']['catchPhrase'] = null;
        $company['company']['bs'] = null;

        if (isset($data['name'])) {
            $company['company']['name'] = $data['name'];
        }
        if (isset($data['catchPhrase'])) {
            $company['company']['catchPhrase'] = $data['catchPhrase'];
        }
        if (isset($data['bs'])) {
            $company['company']['bs'] = $data['bs'];
        }

        return $company;
    }

}
