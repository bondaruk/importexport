<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Geo;
use App\Models\User;

class Address extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street', 'suite', 'city', 'zipcode'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'street', 'suite', 'city', 'zipcode', 'geo_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /****************
     * Attributes
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /****************
     * Relations
     */

    public function geo()
    {
        return $this->hasOne(Geo::class, 'geo_id', 'id')->withTimestamps();
    }

    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }

    /****************
     * Scopes
     */

    /****************
     * Queries
     */

    public static function create($data, $geo_id)
    {
        $address = new Address();

        $address->geo_id = $geo_id;
        $address->street = $data['address']['street'];
        $address->suite = $data['address']['suite'];
        $address->city = $data['address']['city'];
        $address->zipcode = $data['address']['zipcode'];

        if( ! $address->save() ) {
            return false;
        }

        return $address;
    }

    public static function edit($data, $id)
    {
        $address = Address::where('id', $id)->first();

        Geo::edit($data, $address->geo_id);

        $address->street = $data['address']['street'];
        $address->suite = $data['address']['suite'];
        $address->city = $data['address']['city'];
        $address->zipcode = $data['address']['zipcode'];

        if( ! $address->save() ) {
            return false;
        }

        return $address;
    }

    public static function parser(array $data)
    {
        $address['address']['street'] = null;
        $address['address']['suite'] = null;
        $address['address']['city'] = null;
        $address['address']['zipcode'] = null;
        return $address;
    }

}
