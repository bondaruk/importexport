<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;

class UsersExport implements FromArray
{
    /**
     * @return string
     */
    public function title(): string
    {
        return 'User';
    }

    public function array(): array
    {
        $collection = [];

        $users = User::select('name', 'email', 'phone', 'company_id')->orderBy('created_at', 'DESC')->get();
        if( count($users)>0 ){
            foreach ($users as $user){
                $array['name'] = $user->name;
                $array['email'] = $user->email;
                $array['phone'] = '.' . $user->phone;
                $array['companyName'] = $user->company->name;

                $collection[] = $array;
            }
        }

        return $collection;
    }
}
