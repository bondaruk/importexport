<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('addresses')) {
            Schema::create('addresses', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('street')->nullable();
                $table->string('suite')->nullable();
                $table->string('city')->nullable();
                $table->string('zipcode')->nullable();
                $table->unsignedBigInteger('geo_id');
                $table->timestamps();
                $table->engine = 'InnoDB';
                $table->foreign('geo_id')->references('id')->on('geos')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
