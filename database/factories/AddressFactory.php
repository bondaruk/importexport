<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Address;
use App\Models\Geo;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'street' => $faker->streetAddress,
        'suite' => $faker->secondaryAddress,
        'city' => $faker->city,
        'zipcode' => $faker->postcode,

        'geo_id' => function () {
            return factory(Geo::class)->create()->id;
        },
    ];
});
